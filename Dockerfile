FROM alpine

RUN apk add --no-cache python3 py3-pip patchelf build-base scons
RUN pip install --upgrade --break-system-packages wheel setuptools pip
RUN pip install --break-system-packages scons
RUN pip install --break-system-packages staticx
